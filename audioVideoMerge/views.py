from django.shortcuts import render

# Create your views here.

#from moviepy.editor import VideoFileClip, concatenate_videoclips, TextClip, CompositeVideoClip
from moviepy.editor import *

from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

import json
import uuid
import base64
import os

from coutoEditor.settings import BASE_URL

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class AudioVideoMerge(APIView):

	def post(self,request):
		if request.method == 'POST':
			#print(request.data, request.FILES, request.headers)
			v = request.data['video'] # type: String
			if v.startswith("https://api.videowiki.pt"):
				v = v.split('videowiki.pt/')[-1]
			a = request.data['audio']
			file_name = str(uuid.uuid4())
			result_file_name = str(uuid.uuid4())
			a1 = a.partition(",")[2]
			audio_data = base64.b64decode(a1)
			with open(os.path.join(BASE_DIR, 'media/audio/' + file_name + '.mp3'), 'wb') as f_aud:
				f_aud.write(audio_data)
			# f = open(file_name, 'w+')
			# f.write(a)
			# f.close()
			video = VideoFileClip(v)
			v_duration = video.duration
			audio = AudioFileClip(os.path.join(BASE_DIR, 'media/audio/' + file_name + '.mp3'))
			video_with_audio = CompositeVideoClip([video.set_audio(audio)])
			print(video_with_audio)
			video_with_audio.write_videofile(os.path.join(BASE_DIR, "media/audio-video-merged/" + result_file_name + ".mp4"))
			res_dict = {
				"url": BASE_URL + "media/audio-video-merged/" + str(result_file_name) + ".mp4"
			}
			print(res_dict)
			return JsonResponse(res_dict)
