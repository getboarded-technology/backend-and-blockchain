from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

import json
import requests
import urllib.parse

class AudioList(APIView):

	audio_list = {
		0: {
			 "name": "Bensound Summer",
			 "url": "media/audio/bensound-summer.mp3"
			},
		1: {
			"name": "Kiss The Sky",
			"url": "media/audio/Kiss_the_Sky.mp3"
			},
		2: {
			"name": "Lifting Dreams",
			"url": "media/audio/Lifting_Dreams.mp3"
			},
		3: {
			"name": "News Room News",
			"url": "media/audio/News_Room_News.mp3"
			},
		4: {
			"name": "Night Snow",
			"url": "media/audio/Night_Snow.mp3"
			},
		5: {
			"name": "Shattered Paths",
			"url": "media/audio/Shattered_Paths.mp3"
			},
		6: {
			"name": "Simple Sonata",
			"url": "media/audio/Simple_Sonata.mp3"
			},
		7: {
			"name": "White River",
			"url": "media/audio/White_River.mp3"
			}
		}

	def get(self, request):
		if request.method == 'GET':
			return JsonResponse(self.audio_list)
