from django.shortcuts import render

# Create your views here.

from .apps import SentencedetectionConfig

from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from googletrans import Translator

import json

class SentenceDetection(APIView):

	sentence_connector = ['further', 'furthermore', 'moreover', 'in addition', 'additionally', 'then', 'also', 'too', 'besides', 'again', 'equally important', 'first, second', 'finally, last',
						  'similarly', 'comparable', 'in the same way', 'likewise', 'as with', 'equally', 'however', 'nevertheless', 'on the other hand', 'on the contrary', 'even so', 'notwithstanding',
						  'alternatively', 'at the same time', 'though', 'otherwise', 'instead', 'nonetheless', 'conversely', 'meanwhile', 'presently', 'at last', 'finally', 'immediately', 'thereafter',
						  'at that time', 'subsequently', 'eventually', 'currently', 'in the meantime', 'in the past', 'hence', 'therefore', 'accordingly', 'consequently', 'thus', 'thereupon', 'as a result',
						  'in consequence', 'so', 'then', 'in short', 'on the whole', 'in other words', 'to be sure', 'clearly', 'anyway', 'in sum', 'after all', 'in general', 'it seems', 'in brief',
						  'for example', 'for instance', 'that is', 'such as', 'as revealed by', 'illustrated by', 'specifically', 'in particular', 'for one thing', 'this can be seen', 'in',
						  'an instance of', 'this', 'there', 'here', 'beyond', 'nearby', 'next to', 'at that point', 'opposite to', 'adjacent to', 'on the other side', 'in the front', 'in the back', 'but']

	def post(self, request):
		if request.method == 'POST':
			c=0
			response_dict={}
			response_dict_short={}
			text = request.data['text']
			translator = Translator()
			transObj = translator.translate(text, dest='en')
			srcLang = transObj.src
			text = transObj.text
			print(text, srcLang)
			break_type = request.data['break_type']
			#print("text",text)
			response = SentencedetectionConfig.nlp(text)
			for sen in list(response.sents):
				trans_to_src = translator.translate(str(sen), dest=srcLang)
				response_dict[c] = str(trans_to_src.text)
				c+=1
			print("sentences", response_dict)

			if break_type == 'short_sentences':
				c=0
				short_sen=""
				for k,v in response_dict.items():
					#print(v)
					words = v.split(" ");
					for w in words:
						print(w)
						if w in self.sentence_connector:
							short_sen = short_sen + " "
							response_dict_short[c] = short_sen[0:-1]
							short_sen=w + " "
							c+=1
						else:
							short_sen = short_sen + w + " "
					response_dict_short[c] = short_sen[0:-1]
					short_sen=""
					c+=1
				print("short****",response_dict_short)
				responseObj = {
					"sentences": response_dict_short,
					"srcLang": srcLang
				}
				return JsonResponse(responseObj)
			else:
				responseObj = {
					"sentences": response_dict,
					"srcLang": srcLang
				}
				return JsonResponse(responseObj)