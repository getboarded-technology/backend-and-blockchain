from django.shortcuts import render

# Create your views here.

from .apps import SummarizationConfig

from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

import json

class Summarization(APIView):

	def post(self, request):
		if request.method == 'POST':
			text = request.data['text']

			sc = SummarizationConfig
			parser = sc.parser(text)
			summarizer = sc.summarizer()

			SENTENCES_COUNT = 5

			sent_dict = {}
			c=0

			for sentence in summarizer(parser.document, SENTENCES_COUNT):
				sent_dict[c] = str(sentence)
				c+=1

			print(sent_dict)

			return JsonResponse(sent_dict)


