from django.contrib import admin
from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from summarization import views

urlpatterns = [
    path('', views.Summarization.as_view())
]