from django.shortcuts import render

# Create your views here.

#from moviepy.editor import VideoFileClip, concatenate_videoclips, TextClip, CompositeVideoClip
from moviepy.editor import *
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
from moviepy.video.tools.subtitles import SubtitlesClip

from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

import json
import uuid
import math
import os

from coutoEditor.settings import BASE_URL

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class videoScript(APIView):

	def clip_chunks(self,txt, txt_position, script_color, bg_color):
		txt_list = txt.split()
		txt_list_chunks = [txt_list[x:x+5] for x in range(0, len(txt_list), 5)]
		print(txt_list_chunks)
		cvc = []
		c = 0
		for tl in txt_list_chunks:
			tl_join = " ".join(tl)
			txt_clip = TextClip(" "+tl_join+" ", color=script_color, bg_color=bg_color, fontsize=35, font='Amiri-regular')
			#txt_clip = txt_clip.on_color(color=(0,0,0), col_opacity=0.6)
			txt_clip = txt_clip
			txt_clip = txt_clip.set_position(('center', txt_position), relative=True)
			txt_clip = txt_clip.set_duration(3)
			cvc.append(txt_clip.set_start(c))
			c += 3
		return cvc

	def post(self,request):
		if request.method == 'POST':
			print(request.data)
			file_name = str(uuid.uuid4())
			targetname = str(uuid.uuid4())
			txt = request.data['sceneScript']
			script_color = request.data['sceneScriptColor']
			bg_color = request.data['sceneBackgroundColor']
			if(script_color == bg_color):
				bg_color = '#FFFFFF'
			# duration of each clip is of 3s
			scene_len = int(math.ceil((len(txt.split()) * 3) / 5))
			print(request.data)
			ffmpeg_extract_subclip(request.data['video'], 0, scene_len, targetname=os.path.join(BASE_DIR, "media/trimmed-videos/" + targetname + ".mp4"))
			video_sub_clip = VideoFileClip(os.path.join(BASE_DIR, "media/trimmed-videos/" + targetname + ".mp4")).resize(width=720)
			
			txt_position = 'center'
			if request.data['sceneScriptPosition'] == 1:
				txt_position = 0.1
			elif request.data['sceneScriptPosition'] == 2:
				txt_position = 'center'
			elif request.data['sceneScriptPosition'] == 3:
				txt_position = 0.8

			cvc = self.clip_chunks(txt, txt_position, script_color, bg_color)
			cvc.insert(0, video_sub_clip)
			print(cvc)
			video_with_title_overlay = CompositeVideoClip(cvc)
			video_with_title_overlay.write_videofile(os.path.join(BASE_DIR, "media/edit-script/" + file_name + ".mp4"))
			res_dict = {
				"url": BASE_URL + "media/edit-script/" + file_name + ".mp4"
			}
			print(res_dict)
			return JsonResponse(res_dict)
