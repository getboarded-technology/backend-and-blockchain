from PIL import Image
from django.core.files import File
from django.shortcuts import render

# Create your views here.

#from moviepy.editor import VideoFileClip, concatenate_videoclips, TextClip, CompositeVideoClip
from moviepy.editor import *

from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

import json
import uuid
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

from videos.models import Video


class videosConcat(APIView):
	permission_classes = [IsAuthenticated]

	def post(self,request):
		if request.method == 'POST':
			# check for authentication
			if not request.user:
				return JsonResponse({
					'message': "not authenticated"
				})
			print(request.user)
			final_video_list = []
			result_file_name = str(uuid.uuid4())
			for vt in request.data['videos']:
				if vt.startswith("https://api.videowiki.pt"):
					vt = vt.split('videowiki.pt/')[-1]
				video = VideoFileClip(os.path.join(BASE_DIR, os.path.join(BASE_DIR, vt)))
				final_video_list.append(video)

			result_video = concatenate_videoclips(final_video_list, method="compose")
			result_video.write_videofile(os.path.join(BASE_DIR, os.path.join(BASE_DIR, "media/concatenated-videos/" + result_file_name + ".mp4")))
			res_dict = {
				"url": "media/concatenated-videos/" + result_file_name + ".mp4"
			}
			thumbnail = result_video.get_frame(1)
			img = Image.fromarray(thumbnail)
			img = img.resize((400, 300), Image.ANTIALIAS)
			img.save(
				"media/concatenated-videos/" + str(result_file_name) + ".png"
			)

			# save video to user
			file1 = open("media/concatenated-videos/" + str(result_file_name) + ".png", "rb")
			file2 = open("media/concatenated-videos/" + str(result_file_name) + ".mp4", "rb")
			Video(
				title=request.data['info']['title'],
				description=request.data['info']['description'],
				script=request.data['info']['script'],
				image=File(file1),
				url=File(file2),
				user=request.user
			).save()

			return JsonResponse(res_dict)
