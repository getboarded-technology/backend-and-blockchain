from django.contrib import admin
from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from keywordExtraction import views

urlpatterns = [
    path('', views.KeywordExtraction.as_view())
]