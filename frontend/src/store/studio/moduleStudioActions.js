/* =========================================================================================
  File Name: moduleAuthActions.js
  Description: Auth Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
========================================================================================== */

import store from '../store';
import axios from '../../axios';
import constants from '../../../constant';

export default {
  // /////////////////////////////////////////////
  // VideoWiki
  // /////////////////////////////////////////////
  summarizeScript({ commit }) {
    return new Promise((resolve, reject) => {
      const dataObj = {
        text: store.state.studio.videoScript
      };
      axios
        .post(constants.apiUrl + '/smz/', dataObj)
        .then(res => {
          console.log('summarized script', res);
          let script = '';
          for (const s in res.data) {
            script = script + res.data[s] + ' ';
          }
          commit('setScript', script);
          resolve(script);
        })
        .catch(err => {
          console.log('error in script summarization', err);
          reject(err);
        });
    });
  },
  sentenceDetection({ commit }) {
    return new Promise((resolve, reject) => {
      const dataObj = {
        text: store.state.studio.videoScript,
        break_type: 'long_sentences'
      };
      axios
        .post(constants.apiUrl + '/sd/', dataObj)
        .then(res => {
          console.log('sentences detected', res);
          commit('setSentences', res.data.sentences);
          commit('setSourceLanguage', res.data.srcLang);
          resolve(res);
        })
        .catch(err => {
          console.log('error in sentence detection', err);
          reject(err);
        });
    });
  },
  keywordExtraction({ commit }) {
    return new Promise((resolve, reject) => {
      const dataObj = {
        sentences: store.state.studio.scenes,
        srcLang: store.state.studio.sourceLanguage
      };
      axios
        .post(constants.apiUrl + '/ke/', dataObj)
        .then(res => {
          console.log('keywords extracted', res);
          commit('setKeywords', res.data);
          resolve(res);
        })
        .catch(err => {
          console.log('error in keyword extraction', err);
          reject(err);
        });
    });
  },
  audioSuggestions({ commit }) {
    return new Promise((resolve, reject) => {
      axios
        .get(constants.apiUrl + '/al/')
        .then(res => {
          console.log('audio suggestions', res);
          commit('setAudios', res.data);
          resolve(res);
        })
        .catch(err => {
          console.log('error in audio suggestion', err);
          reject(err);
        });
    });
  },
  audioVideoMerge({ commit }, dataObj) {
    return new Promise((resolve, reject) => {
      console.log(dataObj);
      const headers = {
        'Content-Type': 'multipart/form-data'
      };
      axios
        .post(constants.apiUrl + '/avm/', dataObj)
        .then(res => {
          console.log('audio video merge', res);
          commit('setVideoWithAudio', res.data.url);
          resolve(res);
        })
        .catch(err => {
          console.log('error in audio video merge', err);
          reject(err);
        });
    });
  },
  videoSuggestions({ commit }) {
    return new Promise((resolve, reject) => {
      const dataObj = {
        keywords: JSON.stringify(store.state.studio.keywords),
        srcLang: store.state.studio.sourceLanguage
      };
      console.log(dataObj);
      axios
        .post(constants.apiUrl + '/vl/', dataObj)
        .then(res => {
          console.log('video suggestions', res);
          commit('setVideos', res.data);
          resolve(res);
        })
        .catch(err => {
          console.log('error in video suggestion', err);
          reject(err);
        });
    });
  },
  searchVideo({ commit }, dataObj) {
    return new Promise((resolve, reject) => {
      console.log(dataObj);
      axios
        .post(constants.apiUrl + '/vs/', dataObj)
        .then(res => {
          console.log('video searched', res);
          commit('setSearchedVideos', res.data);
          resolve(res);
        })
        .catch(err => {
          console.log('error in video search', err);
          reject(err);
        });
    });
  },
  editSceneScript({ commit }, sceneScriptData) {
    return new Promise((resolve, reject) => {
      axios
        .post(constants.apiUrl + '/vsrt/', sceneScriptData)
        .then(res => {
          console.log('edit scene script', res);
          // commit("setVideos", res.data)
          resolve(res);
        })
        .catch(err => {
          console.log('error in edit scene script', err);
          reject(err);
        });
    });
  },
  concatenateVideos({ commit }, videoWikiData) {
    return new Promise((resolve, reject) => {
      /* const dataObj = {
        videos
      } */
      axios
        .post(constants.apiUrl + '/vc/', videoWikiData)
        .then(res => {
          console.log('video concat', res);
          // commit("setVideos", res.data)
          resolve(res);
        })
        .catch(err => {
          console.log('error in video concat', err);
          reject(err);
        });
    });
  }
};
