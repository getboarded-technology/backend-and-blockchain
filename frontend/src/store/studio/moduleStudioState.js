/* =========================================================================================
  File Name: moduleAuthState.js
  Description: Auth Module State
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
========================================================================================== */

export default {
  video: {
    title: '',
    description: '',
    script: '',
    image: '',
    url: ''
  },
  videoScript: '',
  sourceLanguage: 'en',
  panel: {
    text: true,
    style: false,
    library: false,
    music: false,
    scenes: false,
    publish: false
  },
  scenes: {},
  activeSceneInLibrary: 1,
  activeSceneInScenes: 1,
  keywords: {},
  videos: {},
  searchedVideos: {},

  currentActiveScene: 1,

  selectedFromLibraryVideos: [],
  videoWidth: null,
  videoHeight: null,
  preparedScenesVideos: [],
  recordedAudios: [],
  addedAudioVideos: [],
  styleVideos: [],
  publishedVideo: null
};
