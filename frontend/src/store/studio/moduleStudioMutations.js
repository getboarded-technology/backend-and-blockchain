/* =========================================================================================
  File Name: moduleAuthMutations.js
  Description: Auth Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
========================================================================================== */

export default {
  SET_VIDEO(state, payload) {
    state.video = payload;
  },
  SET_VIDEO_ATTR(state, payload) {
    state.video[payload.key] = payload.value;
  },
  toggleText(state) {
    state.panel.text = true;
    state.panel.style = false;
    state.panel.library = false;
    state.panel.music = false;
    state.panel.scenes = false;
    state.panel.publish = false;
  },
  toggleStyle(state) {
    state.panel.text = false;
    state.panel.style = true;
    state.panel.library = false;
    state.panel.music = false;
    state.panel.scenes = false;
    state.panel.publish = false;
  },
  toggleLibrary(state) {
    state.panel.text = false;
    state.panel.style = false;
    state.panel.library = true;
    state.panel.music = false;
    state.panel.scenes = false;
    state.panel.publish = false;
  },
  toggleMusic(state) {
    state.panel.text = false;
    state.panel.style = false;
    state.panel.library = false;
    state.panel.music = true;
    state.panel.scenes = false;
    state.panel.publish = false;
  },
  toggleScenes(state) {
    state.panel.text = false;
    state.panel.style = false;
    state.panel.library = false;
    state.panel.music = false;
    state.panel.scenes = true;
    state.panel.publish = false;
  },
  togglePublish(state) {
    state.panel.text = false;
    state.panel.style = false;
    state.panel.library = false;
    state.panel.music = false;
    state.panel.scenes = false;
    state.panel.publish = true;
  },
  setScript(state, value) {
    state.videoScript = value;
  },
  setSentences(state, value) {
    state.scenes = value;
  },
  setSourceLanguage(state, value) {
    state.sourceLanguage = value;
  },
  editSceneScript(state, value) {
    state.scenes[state.currentActiveScene - 1] = value;
  },
  setKeywords(state, value) {
    state.keywords = value;
  },
  /* setAudios(state, value) {
    state.videoWiki.audios = value
  }, */
  setVideos(state, value) {
    state.videos = value;
  },
  setInitialVideo(state, dataObj) {
    state.selectedFromLibraryVideos[dataObj.indexs + 1] = dataObj.value;
  },
  setSearchedVideos(state, value) {
    state.searchedVideos = value;
  },
  removeSearchedVideos(state) {
    state.searchedVideos = {};
  },
  setVideoWidth(state, value) {
    state.videoWidth = value;
  },
  setVideoHeight(state, value) {
    state.videoHeight = value;
  },
  setRecordedAudio(state, value) {
    state.recordedAudios[value.sceneNum] = value;
  },
  setVideoWithAudio(state, value) {
    state.addedAudioVideos[state.currentActiveScene] = value;
  },
  setScriptSceneVideo(state, value) {
    state.preparedScenesVideos[state.currentActiveScene] = value;
  },
  setPrepareAllScenes(state, value) {
    state.preparedScenesVideos = value;
  },
  setActiveScene(state, value) {
    state.currentActiveScene = value;
  },
  selectVideo(state, v) {
    state.selectedFromLibraryVideos[0] = -1;
    state.selectedFromLibraryVideos.splice(state.currentActiveScene, 1);
    state.selectedFromLibraryVideos.splice(state.currentActiveScene, 0, v);
  },
  selectAudio(state, a) {
    state.recordedAudios[0] = -1;
    state.recordedAudios.splice(state.currentActiveScene, 1);
    state.recordedAudios.splice(state.currentActiveScene, 0, a);
  },
  setPublishedVideo(state, v) {
    state.publishedVideo = v;
  }
};
