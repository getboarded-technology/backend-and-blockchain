export default {
  studio: {
    errors: {
      e1: 'Your browser does not support the video tag.'
    },
    sidebarPanel: {
      sp1: 'Text',
      sp2: 'Library',
      sp3: 'Format',
      sp4: 'Course',
      sp5: 'Voice',
      sp6: 'Publish',
      sp7: 'Add a video script',
      sp8: 'Select videos for your script',
      sp9: 'Choose a video format',
      sp10: 'Prepare your scenes/course',
      sp11: 'Add voice/narration to your scenes/course',
      text: {
        t1: 'When you create scenes, they will appear here'
      },
      library: {
        l1: 'When you create scenes, video suggestions will appear here',
        l2: 'Scene',
        l3: 'Select one video to continue',
        l4: 'Search...'
      },
      format: {
        f1: 'Aspect Ratio',
        f2: 'Resolution'
      },
      course: {
        c1: 'Prepare All Scenes',
        c2: 'Your browser does not support the video tag.'
      },
      voice: {},
      publish: {
        p1: 'Publish Panel'
      }
    },
    text: {
      t1: 'Create A Video',
      t2: 'Video Title',
      t3: 'Video Description',
      t4: 'Video Script',
      t5: 'Enter Your Video Script here...',
      t6: 'Create Scenes'
    },
    course: {
      c1: 'Script Position',
      c2: 'Text',
      c3: 'BG',
      c4: 'Edit scene script',
      c5: 'Add Text Overlay to Scene'
    },
    voice: {
      v1: 'Start Recording',
      v2: 'Upload & Add Voice',
      v3: 'Stop recording',
      v4: 'Try Again'
    },
    publish: {
      p1: 'Publish Video'
    }
  }
};
