const constants = {
  apiUrl: "https://api.videowiki.pt",
  url: "http://localhost:8080"
}

if (process.env.NODE_ENV === "production"){
  constants.apiUrl = "https://api.videowiki.pt"
  constants.url = "https://videowiki.pt"
}

export default constants;
