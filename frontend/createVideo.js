import web3 from './web3';
import CreateVideo from './build/CreateVideo.json';

const instance = new web3.eth.Contract(
  JSON.parse(CreateVideo.interface),
  '0x07C7C17c807b81B75e7EB197f7342d41A4b82DF9'
);

export default instance;