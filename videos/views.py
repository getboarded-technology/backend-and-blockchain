from django.http import JsonResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from videos.models import Video
from rest_framework.serializers import ModelSerializer


class VideoSerializer(ModelSerializer):
     class Meta:
        model = Video
        fields = '__all__'
        depth = 1


class VideoViewSet(ModelViewSet):
    # countryUpdate()
    queryset = Video.objects.all()
    serializer_class = VideoSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('id', 'user', 'slug')
    # pagination_class = PageNumberPagination
    #
    # def filter_queryset(self, queryset):
    #     gte = self.request.GET.get('match_start_date')
    #     lte = self.request.GET.get('match_end_date')
    #     if gte and lte:
    #         return Match.objects.filter(start_date__gte=gte).filter(start_date__lte=lte).order_by('start_date')
    #     return super().filter_queryset(queryset)



class HomeVidoes(APIView):

    def get(self, request):
        if request.method == 'GET':
            videos = {
                'data': list(Video.objects.all()[:6].values(
                    'id', 'title', 'slug', 'description', 'image', 'url',
                    'script', 'user__first_name', 'user__last_name', 'rating',
                    'time', 'topic'
                ))
            }
            return JsonResponse(videos)


class UserVidoes(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        if request.method == 'GET':
            vidoes = {
                'data': list(Video.objects.filter(user=request.user).values(
                    'id', 'title', 'slug', 'description', 'image', 'url',
                    'script', 'user__first_name', 'user__last_name', 'rating',
                    'time', 'topic'
                )),
            }
            return JsonResponse(vidoes)
