from django.db import models

# Create your models here.
from django.template.defaultfilters import slugify

from user.models import User


class Video(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    description = models.CharField(max_length=1000)
    image = models.FileField(upload_to='videos/images/%Y/%m/%d')
    url = models.FileField(upload_to='videos/images/%Y/%m/%d')
    script = models.TextField()
    topic = models.CharField(max_length=100, default="Education")
    rating = models.FloatField(default=0.0)
    time = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)

        return super(Video, self).save(*args, **kwargs)
class Meta():
    unique_together = (('user','video'),)
    index_together = (('user','video'),)