from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from videos.views import HomeVidoes, UserVidoes, VideoViewSet

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'videos', VideoViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('home_videos', HomeVidoes.as_view()),
    path('user_videos', UserVidoes.as_view())
]